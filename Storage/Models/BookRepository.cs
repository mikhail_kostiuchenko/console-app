﻿using Storage.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Storage.Models
{
    public class BookRepository : EFRepositoryBase<Context, Book, int>, IBookRepository
    {
    }
}
