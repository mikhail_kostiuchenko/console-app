﻿namespace Storage.Models
{
    public class Book
    {
        public int Id { get; set; }

        public string  Name { get; set; }

        public string Autor { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return string.Concat(Id, " ", Name, " ", Autor, " ", Description);
        }
    }
}
