﻿using Storage.Helpers;
using Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using ExceptionHandler = Storage.Helpers.ExceptionHandler;

namespace Storage.Models
{
    public abstract class EFRepositoryBase<TContext, TEntity, TKey> : IRepository<TEntity, TKey>
                          where TEntity : class where TContext : DbContext
    {
        private readonly DbContext m_dbContext;
        public EFRepositoryBase()
            : this(Activator.CreateInstance<TContext>())
        {

        }
        public EFRepositoryBase(DbContext context)
        {
            m_dbContext = context;
           
        }


        public ICollection<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return GetSet().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                return new List<TEntity>();
            }
        }

        public TEntity Get(TKey id)
        {
            try
            {
                return GetSet().Find(id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ICollection<TEntity> GetAll()
        {
            return ExceptionHandler.ExecHandler(() => GetSet().ToList());
            //try
            //{
            //    return GetSet().ToList();
            //}
            //catch (Exception)
            //{
            //    return new List<TEntity>();
            //}
        }

        public void Insert(TEntity model)
        {
            ExceptionHandler.ExecHandler(() => GetSet().Add(model));
            ExceptionHandler.ExecHandler(() => m_dbContext.SaveChanges());
        }

        public bool Remove(TKey id)
        {
            try
            {
                var entity = GetSet().Find(id);
                GetSet().Remove(entity);
                m_dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool Delete(TKey id)
        {
            try
            {
                var model = Get(id);
                GetSet().Remove(model);
                m_dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(TEntity model)
        {
            try
            {
                GetSet().Attach(model);
                m_dbContext.Entry(model).State = EntityState.Modified;
                m_dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected DbSet<TEntity> GetSet()
        {
            return m_dbContext.Set<TEntity>();
        }

        //public TResult ExecHandler<TResult>(Func<TResult> func)
        //{
        //    try
        //    {
        //        return func();
        //    }
        //    catch (Exception exception)
        //    {
        //        throw new Exception(exception.Message);
        //    }
        //}
    }
}
