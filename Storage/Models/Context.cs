﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Models
{
    public class Context : DbContext
    {
        public Context() : base("DbConnection")
        {
        }

        public DbSet<Book> Books { get; set; }
    }
}
 