﻿using Storage.Models;

namespace Storage.Interfaces
{
    public interface IBookRepository : IRepository<Book, int>
    {
    }
}
