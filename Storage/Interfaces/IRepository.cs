﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Interfaces
{
    public interface IRepository<TEntity, TKey> where TEntity : class
    {
        ICollection<TEntity> GetAll();

        ICollection<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        TEntity Get(TKey id);

        void Insert(TEntity model);

        bool Update(TEntity model);

        bool Remove(TKey id);

        bool Delete(TKey id);
    }
}
