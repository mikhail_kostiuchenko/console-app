﻿using Storage.Models;

namespace Storage.Interfaces
{
    public interface ISerializator<TModel> where TModel : class 
    {
        void Save();
        void Load();
    }
}
