﻿using Newtonsoft.Json;
using Storage.Interfaces;
using Storage.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace Storage.Utils
{
    public class Serializator<TModel> : ISerializator<TModel> where TModel : class
    {
        private string _filename;
        private ICollection<TModel> _models;

        public Serializator(string FileName)
        {
            _filename = FileName;
        }

        public ICollection<TModel> Data
        {
            get
            {
                try
                {
                    if (_models == null)
                       Load();
                }
                catch (Exception)
                {

                    _models = new List<TModel>();
                }
                return _models;
            }
            set
            {
                _models = value;
                Save();
            }
        }

        public void Save()
        {

            using (StreamWriter sw = new StreamWriter(_filename))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(sw, _models);
            }
        }

        public void Load()
        {
            using (StreamReader file = File.OpenText(_filename))
            {
                JsonSerializer serializer = new JsonSerializer();
                _models = (List<TModel>)serializer.Deserialize(file, typeof(List<TModel>));
            }
        }
    }
}
