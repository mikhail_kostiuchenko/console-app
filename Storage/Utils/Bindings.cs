﻿using Ninject.Modules;
using Storage.Interfaces;
using Storage.Menu;
using Storage.Models;
using System.Reflection.Emit;

namespace Storage.Utils
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IBookRepository>().To<BookRepository>();
            Bind<ISerializator<Book>>().To<Serializator<Book>>();
            Bind<IMainMenu>().To<MainMenu>();
        }
    }
}
