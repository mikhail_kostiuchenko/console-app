﻿using Storage.Interfaces;
using Storage.Models;
using Storage.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Storage.Menu
{
    public class MainMenu : IMainMenu
    {
        private readonly IBookRepository _repo;
        private readonly Dictionary<int, Action> _dict;
        bool isRun = true;

        public MainMenu(IBookRepository repo)
        {
            _repo = repo;
            _dict = new Dictionary<int, Action>
            {
                {1, () => GetAll() },
                {2, () => CreateItem() },
                {3, () => DeleteItems() },
                {0, () => Exit() }
            };
        }

        public void LoadMenu()
        {
            while (isRun)
            {
                Console.Clear();
                Console.WriteLine("    Welcome");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("1 - List of Books");
                Console.WriteLine("2 - Create a Book");
                Console.WriteLine("3 - Delete all Books");
                Console.WriteLine("0 - Exit");
                Console.Write(">>");
                string inputKey = Console.ReadLine();
                Console.Clear();

                if (_dict.ContainsKey(int.Parse(inputKey)))
                {
                    _dict[int.Parse(inputKey)]();
                }
            }            
        }

        private void GetAll()
        {
            var items = _repo.GetAll();
            foreach (var item in items)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }

        private void CreateItem()
        {
            Book book = new Book();
            Console.Write("Name: ");
            book.Name = Console.ReadLine();
            Console.Write("Autor: ");
            book.Autor = Console.ReadLine();
            Console.Write("Description: ");
            book.Description = Console.ReadLine();
            Console.Clear();
            try
            {
                _repo.Insert(book);
                Console.WriteLine("Book added!");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                Console.ReadKey();
            }
        }

        private void DeleteItems()
        {
            var items = _repo.GetAll();
            foreach (var item in items)
            {
                _repo.Remove(item.Id);
            }
            Console.WriteLine("All books was deleted!");
            Console.ReadLine();
        }

        private void Exit()
        {
            isRun = false;
        }
    }
}
