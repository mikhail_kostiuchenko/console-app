﻿using Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Helpers
{
    public  static class ExceptionHandler 
    {
        public static TResult ExecHandler<TResult>(Func<TResult> func)
        {
            try
            {
                return func();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}
